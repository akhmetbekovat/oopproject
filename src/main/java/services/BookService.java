package services;

import domain.models.Book;
import repositories.BookRepository;
import repositories.interfaces.IBookRepository;
import services.interfaces.IBookService;

import java.util.List;

public class BookService implements IBookService {
    private IBookRepository bookRepo = new BookRepository();
    @Override
    public Book getBookByID(long book_id) {
        return bookRepo.getBookByID(book_id);
    }

    @Override
    public void addBook(Book book) {
        bookRepo.add(book);
    }

    @Override
    public void updateBook(Book book) {
        bookRepo.update(book);
    }

    @Override
    public void removeBook(Book book) {
        bookRepo.remove(book);
    }

    @Override
    public List<Book> showAllBooks() {
        return bookRepo.show();
    }

}
