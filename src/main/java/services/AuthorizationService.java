package services;

import domain.AccessToken;

import domain.StudentLoginData;
import domain.models.Student;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import repositories.StudentRepository;
import repositories.interfaces.IStudentRepository;
import services.interfaces.IAuthorizationService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;


public class AuthorizationService implements IAuthorizationService {

    private final IStudentRepository userRepository = new StudentRepository();

    public AccessToken authenticateStudent(StudentLoginData data) throws Exception {
        Student authenticatedStudent = signIn(data);
        return new AccessToken(issueToken(authenticatedStudent));
    }

    @Override
    public Student getStudentByEmail(String issuer) {
        return userRepository.getStudentByEmail(issuer);
    }

    private Student signIn(StudentLoginData data) throws Exception {
        Student student = userRepository.findStudentByLogin(data);
        if (student == null) {
            throw new Exception("Authentication failed!");
        }
        return student;
    }

    private String issueToken(Student student) { // ??????
        Instant now = Instant.now();
        String secretWord = "TheStrongestSecretKeyICanThinkOf";
        return Jwts.builder()
                .setIssuer(student.getEmail())
                .setIssuedAt(Date.from(now))
                .claim("1d20", new Random().nextInt(20) + 1)
                .setExpiration(Date.from(now.plus(10, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secretWord.getBytes()))
                .compact();
    }
}
