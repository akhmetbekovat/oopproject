package services;

import domain.models.Issue;
import repositories.IssueRepository;
import repositories.interfaces.IIssueRepository;
import services.interfaces.IIssueService;

import java.util.List;

public class IssueService implements IIssueService {
    private IIssueRepository issueRepo= new IssueRepository();

    @Override
    public Issue getIssueByID(long issue_id) {
        return issueRepo.getIssueByID(issue_id);
    }

    @Override
    public void addIssue(Issue issue) {
issueRepo.add(issue);
    }



    @Override
    public void removeIssue(Issue issue) {
issueRepo.remove(issue);
    }

    @Override
    public List<Issue> showAllIssues() {
        return issueRepo.show();
    }
}
