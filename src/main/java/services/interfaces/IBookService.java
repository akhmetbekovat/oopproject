package services.interfaces;


import domain.models.Book;

import java.util.List;

public interface IBookService {
    Book getBookByID(long book_id);


    void addBook(Book book);

    void updateBook(Book book);

    void removeBook(Book book);

    List<Book> showAllBooks();


}
