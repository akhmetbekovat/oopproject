package services.interfaces;


import domain.models.Issue;

import java.util.List;

public interface IIssueService {
    Issue getIssueByID(long issue_id);


    void addIssue(Issue issue);
void updateIssue(Issue issue);

    void removeIssue(Issue issue);

    List<Issue> showAllIssues();

}

