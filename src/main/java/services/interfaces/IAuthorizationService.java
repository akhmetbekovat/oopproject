package services.interfaces;

import domain.AccessToken;
import domain.StudentLoginData;
import domain.models.Student;


public interface IAuthorizationService {
    AccessToken authenticateStudent(StudentLoginData data) throws Exception;

    Student getStudentByEmail(String issuer);
}
