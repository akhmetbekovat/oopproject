package services.interfaces;

import domain.models.Student;

import java.util.List;

public interface IStudentService {
    Student getStudentByID(long student_id);

    Student  getStudentByEmail(String email);

    void addStudent(Student student);

    void updateStudent(Student student);

    void removeStudent(Student student);
List<Student> showAllStudents();
}
