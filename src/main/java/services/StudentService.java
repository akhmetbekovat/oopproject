package services;

import domain.models.Student;
import repositories.StudentRepository;
import repositories.interfaces.IStudentRepository;
import services.interfaces.IStudentService;

import java.util.List;

public class StudentService implements IStudentService {
    private IStudentRepository studentRepo = new StudentRepository();
@Override
    public Student getStudentByID(long student_id) {
        return studentRepo.getStudentByID(student_id);
    }
@Override
    public Student getStudentByEmail(String email){
        return studentRepo.getStudentByEmail(email);
    }
@Override
    public void addStudent(Student student) {
        studentRepo.add(student);
    }
    @Override
    public void removeStudent(Student student){studentRepo.remove(student);}

    @Override
    public List<Student> showAllStudents() {
        return studentRepo.show();
    }

    @Override
    public void updateStudent(Student student) {
        studentRepo.update(student);
    }

}
