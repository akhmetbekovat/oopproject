package domain.models;


import domain.DTOs.StudentDTO;

public class Forfun {
    private String fname;
    private StudentDTO holder;

    public Forfun() {}

    public Forfun(String fname, StudentDTO holder) {
        setFname(fname);
        setHolder(holder);
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public StudentDTO getHolder() {
        return holder;
    }

    public void setHolder(StudentDTO holder) {
        this.holder = holder;
    }
}
