package domain.models;

public class Book {
    private long book_id;
    private String name;
    private String author;
    private String description;

    public Book(){

    }

    public Book( String name, String author, String description) {
        setName(name);
        setAuthor(author);
        setDescription(description);
    }
    public Book(long book_id, String name, String author, String description) {
        setBook_id(book_id);
        setName(name);
        setAuthor(author);
        setDescription(description);
    }

    public long getBook_id() {
        return book_id;
    }

    public void setBook_id(long book_id) {
        this.book_id = book_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
