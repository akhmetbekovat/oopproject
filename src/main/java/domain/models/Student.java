package domain.models;

public class Student {
    private long student_id;
    private String fname;
    private String lname;
    private String email;
    private String password;
    private String department;
    private String role;

    public Student(){

    }

    public Student(long student_id, String fname, String lname, String email, String password, String department, String role) {
        this.student_id = student_id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.password = password;
        this.department = department;
        this.role = role;
    }

    public Student(long student_id, String fname, String lname, String email, String department, String role) {
        this.student_id = student_id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.department = department;
        this.role = role;
    }

    public Student(String fname, String lname, String email, String password, String department, String role) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.password = password;
        this.department = department;
        this.role = role;
    }

    public long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Student{" +
                "student_id=" + student_id +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", department='" + department + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
