package domain.models;

import java.util.Date;

public class Issue {
    private long issue_id;
    private long student_id;
    private long book_id;
    private Date borrowed_date;
    private Date return_date;

    public Issue(){

    }

    public Issue(long issue_id, long student_id, long book_id, Date borrowed_date, Date return_date) {
        setIssue_id(issue_id);
        setStudent_id(student_id);
        setBook_id(book_id);
        setBorrowed_date(borrowed_date);
        setReturn_date(return_date);
    }
    public Issue(long student_id, long book_id, Date borrowed_date, Date return_date) {
        setStudent_id(student_id);
        setBook_id(book_id);
        setBorrowed_date(borrowed_date);
        setReturn_date(return_date);
    }

    public long getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(long issue_id) {
        this.issue_id = issue_id;
    }

    public long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public long getBook_id() {
        return book_id;
    }

    public void setBook_id(long book_id) {
        this.book_id = book_id;
    }

    public java.sql.Date getBorrowed_date() {
        return (java.sql.Date) borrowed_date;
    }

    public void setBorrowed_date(Date borrowed_date) {
        this.borrowed_date = borrowed_date;
    }

    public Date getReturn_date() {
        return return_date;
    }

    public void setReturn_date(Date return_date) {
        this.return_date = return_date;
    }
}

