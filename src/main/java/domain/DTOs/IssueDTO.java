package domain.DTOs;


import domain.models.Issue;

import java.util.Date;

public class IssueDTO {
    private long student_id;
    private long book_id;
    private Date borrowed_date;
    private Date return_date;

    public IssueDTO( long student_id, long book_id, Date borrowed_date, Date return_date) {
        setStudent_id(student_id);
        setBook_id(book_id);
        setBorrowed_date(borrowed_date);
        setReturn_date(return_date);
    }

    public IssueDTO(Issue issue) {
    }


    public long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public long getBook_id() {
        return book_id;
    }

    public void setBook_id(long book_id) {
        this.book_id = book_id;
    }

    public java.sql.Date getBorrowed_date() {
        return (java.sql.Date) borrowed_date;
    }

    public void setBorrowed_date(Date borrowed_date) {
        this.borrowed_date = borrowed_date;
    }

    public Date getReturn_date() {
        return return_date;
    }

    public void setReturn_date(Date return_date) {
        this.return_date = return_date;
    }
}
