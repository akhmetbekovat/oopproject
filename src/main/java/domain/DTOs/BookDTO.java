package domain.DTOs;

import domain.models.Book;

public class BookDTO {
    private String name;
    private String author;
    private String description;

    public BookDTO(String name, String author, String description) {
       setName(name);
       setAuthor(author);
       setDescription(description);
    }

    public BookDTO(Book book) {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
