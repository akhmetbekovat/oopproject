package domain.DTOs;



import domain.models.Student;


public class StudentDTO {
    private String fname;
    private String lname;
    private String email;
    private String department;

    public StudentDTO(Student student) {
        setFname(student.getFname());
        setLname(student.getLname());
        setDepartment(student.getDepartment());
        setEmail(student.getEmail());
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
