package repositories;

import domain.StudentLoginData;
import domain.models.Student;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IStudentRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class StudentRepository implements IStudentRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void add(Student entity) {
        try {
            String sql = "INSERT INTO student(fname, lname, email, password, department, role) " +
                    "VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getFname());
            stmt.setString(2, entity.getLname());
            stmt.setString(3, entity.getEmail());
            stmt.setString(4, entity.getPassword());
            stmt.setString(5, entity.getDepartment());
            stmt.setString(6, entity.getRole());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Student entity) { // Spring
        String sql = "UPDATE student SET ";

        if (entity.getFname() != null)
            sql += "fname=?,";
        if (entity.getLname() != null)
            sql += "lname=?,";
        if (entity.getPassword() != null)
            sql += "password=?,";
        if (entity.getDepartment() != null)
            sql += "department=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE email=?";

        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getFname() != null)
                stmt.setString(i++, entity.getFname());
            if (entity.getLname() != null)
                stmt.setString(i++, entity.getLname());
            if (entity.getPassword() != null)
                stmt.setString(i++, entity.getPassword());
            if (entity.getDepartment() != null)
                stmt.setString(i++, entity.getDepartment());
            stmt.setString(i++, entity.getEmail());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Student entity) {
        try {
            String sql = "DELETE FROM student WHERE student_id = ?";
            PreparedStatement stmt = null;
            stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,entity.getStudent_id());
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Student> show() {
        try {
            Statement stmt = null;
            String query =
                    "select * from student";

            stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                long student_id = rs.getLong("student_id");
                String fname = rs.getString("fname");
                String lname = rs.getString("lname");
                String email = rs.getString("email");
                String department = rs.getString("department");
                System.out.println(student_id + "\t" + fname +
                        "\t" + lname + "\t" + email +
                        "\t" + department);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Student> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Student> students = new LinkedList<>();
            while (rs.next()) {
                Student student = new Student(
                        rs.getLong("student_id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("email"),
                        //rs.getString("password"),
                        rs.getString("department"),
                        rs.getString("role")
                );
                students.add(student);
            }
            return students;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Student queryOne(String sql) { // ОРМ ????
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Student(
                        rs.getLong("student_id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("email"),
                        rs.getString("department"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public Student getStudentByID(long student_id) {
        String sql = "SELECT * FROM student WHERE student_id = " + student_id + " LIMIT 1";
        return queryOne(sql);
    }

    public Student findStudentByLogin(StudentLoginData data) {
        try {
            String sql = "SELECT * FROM student WHERE email = ? AND password = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getEmail());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Student(
                        rs.getLong("student_id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("department"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public Student getStudentByEmail(String email) {
        try {
            String sql = "SELECT * FROM student WHERE email = ? LIMIT 1";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Student(
                        rs.getLong("student_id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("department"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

}
