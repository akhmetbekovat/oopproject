package repositories;

import domain.models.Book;
import repositories.interfaces.IBookRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class BookRepository implements IBookRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public Book getBookByID(long book_id) {
        String sql = "SELECT * FROM book WHERE book_id = " + book_id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public void add(Book entity) {
        try {
            String sql = "INSERT INTO book(name, author, description) " +
                    "VALUES(?, ?, ?, ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getAuthor());
            stmt.setString(3, entity.getDescription());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Book entity) {
        String sql = "UPDATE book SET ";

        if (entity.getName() != null)
            sql += "name=?,";
        if (entity.getAuthor() != null)
            sql += "author=?,";
        if (entity.getDescription() != null)
            sql += "description=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE book_id=?";

        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getName() != null)
                stmt.setString(i++, entity.getName());
            if (entity.getAuthor() != null)
                stmt.setString(i++, entity.getAuthor());
            if (entity.getDescription() != null)
                stmt.setString(i++, entity.getDescription());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Book entity) {
        try {
            String sql = "DELETE FROM book WHERE book_id = ?";
            PreparedStatement stmt = null;
            stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,entity.getBook_id());
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Book> show() {
        try {
            Statement stmt = null;
            String query =
                    "select * from book";

            stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                long book_id = rs.getLong("book_id");
               String name = rs.getString("name");
                String author = rs.getString("author");
                String description = rs.getString("description");

                System.out.println(book_id + "\t" +name +
                        "\t" + author+ "\t" +description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Book> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Book> books = new LinkedList<>();
            while (rs.next()) {
                Book book = new Book(
                        rs.getLong("book_id"),
                        rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("description")
                );
                books.add(book);
            }
            return books;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Book queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Book(
                        rs.getLong("book_id"),
                        rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("description")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }
}
