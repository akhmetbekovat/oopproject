package repositories;

import domain.models.Issue;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IIssueRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class IssueRepository implements IIssueRepository {
    private IDBRepository dbrepo = new PostgresRepository();
    @Override
    public Issue getIssueByID(long issue_id) {
        String sql = "SELECT * FROM issue WHERE issue_id = " + issue_id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public void add(Issue entity) {
        try {
            String sql = "INSERT INTO issue(student_id, book_id, borrowed_date, return_date) " +
                    "VALUES(?, ?, ?, ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, entity.getStudent_id());
            stmt.setLong(2, entity.getBook_id());
            stmt.setDate(3, entity.getBorrowed_date());
            stmt.setDate(4, (Date) entity.getReturn_date());
            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Issue entity) {

    }

    @Override
    public void remove(Issue entity) {
        try {
            String sql = "DELETE FROM issue WHERE issue_id = ?";
            PreparedStatement stmt = null;
            stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,entity.getIssue_id());
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Issue> show() {
        try {
            Statement stmt = null;
            String query =
                    "select * from issue";

            stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                long issue_id = rs.getLong("issue_id");
                long book_id = rs.getLong("book_id");
                long student_id = rs.getLong("student_id");
                Date borrowed_date = rs.getDate("borrowed_date");
                Date return_date = rs.getDate("return_date");
                System.out.println(issue_id + "\t" +book_id +
                        "\t" + student_id + "\t" + borrowed_date +
                        "\t" + return_date);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Issue> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Issue> issues = new LinkedList<>();
            while (rs.next()) {
                Issue issue = new Issue(
                        rs.getLong("issue_id"),
                        rs.getLong("book_id"),
                        rs.getLong("student_id"),
                        rs.getDate("borrowed_date"),
                        rs.getDate("return_date")
                );
                issues.add(issue);
            }
            return issues;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Issue queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Issue(
                        rs.getLong("issue_id"),
                        rs.getLong("book_id"),
                        rs.getLong("student_id"),
                        rs.getDate("borrowed_date"),
                        rs.getDate("return_date")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }
}
