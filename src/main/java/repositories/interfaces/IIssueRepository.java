package repositories.interfaces;

import domain.models.Issue;




public interface IIssueRepository extends IEntityRepository <Issue> {
    Issue getIssueByID(long issue_id);


}
