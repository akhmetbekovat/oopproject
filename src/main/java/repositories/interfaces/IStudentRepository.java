package repositories.interfaces;

import domain.StudentLoginData;
import domain.models.Student;

public interface IStudentRepository extends IEntityRepository <Student>{
    Student getStudentByID(long student_id);

    Student findStudentByLogin(StudentLoginData data);

    Student getStudentByEmail(String email);

}
