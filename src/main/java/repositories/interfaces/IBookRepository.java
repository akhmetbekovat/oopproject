package repositories.interfaces;


import domain.models.Book;

public interface IBookRepository extends IEntityRepository <Book> {
    Book getBookByID(long book_id);
}
