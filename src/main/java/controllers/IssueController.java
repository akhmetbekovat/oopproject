package controllers;


import domain.DTOs.IssueDTO;


import domain.models.Issue;

import filters.customAnnotations.JWTTokenNeeded;
import filters.customAnnotations.OnlyAdmin;


import services.IssueService;

import services.interfaces.IIssueService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("issues")
public class IssueController {
    private IIssueService issueService = new IssueService();
    @GET
    public String index(){
        return "Hello from Issue Controller!";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{issue_id}")
    public Response getIssueByID(@PathParam("issue_id") long issue_id) {
        Issue issue;
        try {
            issue = issueService.getIssueByID(issue_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This issue cannot be created").build();
        }


        if (issue == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Issue does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(new IssueDTO(issue))
                    .build();
        }
    }
    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/show")
    public Response showAllIssues() {
        try {
            issueService.showAllIssues();
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Here is the list of issues!")
                .build();
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewIssue(Issue issue) {

        try {
            issueService.addIssue(issue);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Issue added successfully!")
                .build();
    }



    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeIssue(@FormDataParam("issue_id") long issue_id,
                                  @FormDataParam("book_id") long book_id,
                                  @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(book_id)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        Issue issue = issueService.getIssueByID(book_id);
        issueService.removeIssue(issue);

        return Response.
                status(Response.Status.OK)
                .entity("Issue deleted successfully!")
                .build();
    }
    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateIsssue(Issue issue, @Context ContainerRequestContext requestContext) {

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(issue.getIssue_id())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            issueService.updateIssue(issue);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("Issue updated successfully!")
                .build();
    }
}


