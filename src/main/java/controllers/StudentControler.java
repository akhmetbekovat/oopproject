package controllers;

import domain.DTOs.StudentDTO;
import domain.models.Student;
import filters.customAnnotations.JWTTokenNeeded;
import filters.customAnnotations.OnlyAdmin;

import services.StudentService;
import services.interfaces.IStudentService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("students")
public class StudentControler {
    private IStudentService studentService = new StudentService();
    @GET
    public String index(){
        return "Hello from Student Controller!";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{student_id}")
    public Response getStudentByID(@PathParam("student_id") long student_id) {
        Student student;
        try {
            student = studentService.getStudentByID(student_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This student cannot be created").build();
        }


        if (student == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Student does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(new StudentDTO(student))
                    .build();
        }
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewUser(Student student) {

        try {
            studentService.addStudent(student);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Student created successfully!")
                .build();
    }

    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/show")
    public Response showAllStudents() {
        try {
            studentService.showAllStudents();
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Here is the list of students!")
                .build();
    }


    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateUser(Student student, @Context ContainerRequestContext requestContext) {

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(student.getEmail())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            studentService.updateStudent(student);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("Student updated successfully!")
                .build();
    }
    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeStudent(@FormDataParam("student_id") long student_id,
                               @FormDataParam("email") String email,
                               @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(email)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        Student student = studentService.getStudentByID(student_id);
        studentService.removeStudent(student);

        return Response.
                status(Response.Status.OK)
                .entity("Student deleted successfully!")
                .build();
    }
}


