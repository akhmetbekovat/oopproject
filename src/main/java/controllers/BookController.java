package controllers;


import domain.DTOs.BookDTO;
import domain.models.Book;

import filters.customAnnotations.JWTTokenNeeded;
import filters.customAnnotations.OnlyAdmin;

import services.BookService;
import services.interfaces.IBookService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("books")
public class BookController {
    private IBookService bookService = new BookService();
    @GET
    public String index(){
        return "Hello from Book Controller!";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{book_id}")
    public Response getBookByID(@PathParam("book_id") long book_id) {
        Book book;
        try {
            book = bookService.getBookByID(book_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This book cannot be created").build();
        }


        if (book == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Book does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(new BookDTO(book))
                    .build();
        }
    }

    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/show")
    public Response showAllBooks() {
        try {
            bookService.showAllBooks();
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Here is the list of books!")
                .build();
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewBook(Book book) {

        try {
            bookService.addBook(book);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Book added successfully!")
                .build();
    }


    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateBook(Book book, @Context ContainerRequestContext requestContext) {

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(book.getName())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            bookService.updateBook(book);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("Book updated successfully!")
                .build();
    }
    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeBook(@FormDataParam("book_id") long book_id,
                                  @FormDataParam("name") String name,
                                  @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(name)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        Book book = bookService.getBookByID(book_id);
        bookService.removeBook(book);

        return Response.
                status(Response.Status.OK)
                .entity("Book deleted successfully!")
                .build();
    }
}


